<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
    echo "<h3>Soal No 1 Looping I Love PHP</h3>";

    echo "<h3>Looping 1</h3>";
    $i = 2;
    while($i<= 20){
        echo "$i - I Love PHP <br>";
        $i+=2;
    }

    echo "<h3>Looping 2</h3>";
    $a = 20;
    do {
        echo "$a - I Love PHP <br>";
        $a -= 2;
    } while ($a >= 1);


    echo "<h3>Soal No 2 Looping Array Modulo </h3>";

    $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        
        foreach($numbers as $value){
            $rest[] = $value % 5;
        }

        echo "<br>";
        echo "Array sisa baginya adalah:  "; 
        print_r($rest);
        echo "<br>";


        echo "<h3> Soal No 3 Looping Asociative Array </h3>";

        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];

        foreach($items as $bio){
            $data = [
                "id" => $bio[0],
                "Nama Barang" => $bio[1],
                "Harga" => $bio[2],
                "Keterangan" => $bio[3],
                "source" => $bio[4],
            ];

            print_r($data);
            echo "<br>";
        }

        echo "<h3>Soal No 4 Asterix </h3>";

        for($j=1; $j<=5; $j++) {
            for($b=1; $b<=$j; $b++) {
                echo "*";
            }
            echo "<br>";
        }
?>
</body>
</html>